package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JFrame;

public class Servidor {

	static ServerSocket servidor;
	static Socket []S=new Socket[20];
	static String []us=new String[20];
	static Socket cli;
	static DataInputStream in;
	static DataOutputStream out;
	static Thread hilo;
	static int ind;
	public static void main(String[] args) {
		
		
		
		try {
			servidor = new ServerSocket(8080);
			System.out.println("Servidor iniciado");
			ind=0;
			while(true){
				cli= servidor.accept();
				S[ind]=cli;
				in = new DataInputStream(cli.getInputStream());
				String Usuario = in.readUTF();
				us[ind]=Usuario;
				hilo = new HiloClientrada(cli,Usuario);
				hilo.start();
				ind++;

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static class HiloClientrada extends Thread{
		Socket cli;
		String User;
		String sms;
		public HiloClientrada(){
			
		}
		public HiloClientrada(Socket x,String y) {
			cli = x;
			User = y;
			sms=null;
		}
		public void run(){
			try {
				DataInputStream in;
				DataOutputStream out;
				in = new DataInputStream(cli.getInputStream());
				out = new DataOutputStream(cli.getOutputStream());
				while(true){
					String mensaje = in.readUTF();
					sms=mensaje;
					if(mensaje.equals("salir")){
						System.out.println(User+" SALIO DEL CHAT");
						cli.close();
						User=null;
						break;
					}
					else{
						System.out.println(User+" : " + mensaje);
					}
					retransmitir();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		public void retransmitir() {
			for (int i = 0; i < ind; i++) {
				if(!us[i].equals(User) && !us[i].equals(null)){
					try {
						
						DataOutputStream out;
						out = new DataOutputStream(S[i].getOutputStream());
						out.writeUTF(User+" : " + sms);
					
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}
	}
}