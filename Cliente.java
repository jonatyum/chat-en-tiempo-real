package Cliente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class Cliente {

    Cliente mainGUI;
    JFrame newFrame = new JFrame();
    JButton sendMessage;
    JTextField messageBox;
    JTextArea chatBox;
    JTextField usernameChooser;
    JFrame preFrame;

    private static DataInputStream in;
    private static DataOutputStream out;
    private static Socket cli;
    private static Thread h;
    
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
        	cli=new Socket("127.0.0.1",8080);
    		in = new DataInputStream(cli.getInputStream());
    		out = new DataOutputStream(cli.getOutputStream());
		} catch (Exception e) {
			// TODO: handle exception
		}
        Cliente mainGUI = new Cliente();
        mainGUI.preDisplay();
    }


    public void preDisplay() {
        newFrame.setVisible(false);
        preFrame = new JFrame("Nombre de Usuario");
        usernameChooser = new JTextField();
        JLabel chooseUsernameLabel = new JLabel("Nombre de Usuario:");
        JButton enterServer = new JButton("Entrar al Chat");
        JPanel prePanel = new JPanel(new GridBagLayout());

        GridBagConstraints preRight = new GridBagConstraints();
        preRight.anchor = GridBagConstraints.EAST;
        GridBagConstraints preLeft = new GridBagConstraints();
        preLeft.anchor = GridBagConstraints.WEST;
        preRight.weightx = 2.0;
        preRight.fill = GridBagConstraints.HORIZONTAL;
        preRight.gridwidth = GridBagConstraints.REMAINDER;

        prePanel.add(chooseUsernameLabel, preLeft);
        prePanel.add(usernameChooser, preRight);
        preFrame.add(BorderLayout.CENTER, prePanel);
        preFrame.add(BorderLayout.SOUTH, enterServer);
        preFrame.setVisible(true);
        preFrame.setSize(300, 300);

        enterServer.addActionListener(new enterServerButtonListener());
    }

    public void display() {
    	h=new Hilito();
    	h.start();
        newFrame.setVisible(true);
        JPanel southPanel = new JPanel();
        newFrame.add(BorderLayout.SOUTH, southPanel);
        southPanel.setBackground(Color.BLUE);
        southPanel.setLayout(new GridBagLayout());

        messageBox = new JTextField(30);
        sendMessage = new JButton("Enviar");
        chatBox = new JTextArea();
        chatBox.setEditable(false);
        newFrame.add(new JScrollPane(chatBox), BorderLayout.CENTER);
        
        chatBox.setLineWrap(true);

        GridBagConstraints left = new GridBagConstraints();
        left.anchor = GridBagConstraints.WEST;
        GridBagConstraints right = new GridBagConstraints();
        right.anchor = GridBagConstraints.EAST;
        right.weightx = 2.0;

        southPanel.add(messageBox, left);
        southPanel.add(sendMessage, right);

        chatBox.setFont(new Font("Serif", Font.PLAIN, 15));
        sendMessage.addActionListener(new sendMessageButtonListener());
        newFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        newFrame.setSize(470, 300);
    }

    class sendMessageButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            if (messageBox.getText().length() < 1) {
                // do nothing 
            } else if (messageBox.getText().equals(".borrar")) {
                chatBox.setText("Mensajes Borrados\n");
                messageBox.setText("");
            } else {
                chatBox.append(username + " :  " + messageBox.getText() + "\n");
                try {
					out.writeUTF(messageBox.getText());
					if(messageBox.getText().equals("salir")){
						cli.close();
						newFrame.setVisible(false);
						preFrame.setVisible(false);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                messageBox.setText("");
            }
        }
    }

    String username;

    class enterServerButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            username = usernameChooser.getText();
            newFrame.setTitle("Cliente: "+username);
            try {
				out.writeUTF(username);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if (username.length() < 1) {System.out.println("No!"); }
            else {
            preFrame.setVisible(false);
            display();
            }
        }

    }
    class Hilito extends Thread{
    	public Hilito(){
    	}
    	public void run(){
			try {
				while(true){
					String mensaje = in.readUTF();
					
					if(mensaje.equals("salir")){
						cli.close();
						chatBox.append("Un miembro salio" + "\n");
						break;
					}
					else{
						chatBox.append(mensaje + "\n");	
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
    	}
    }
}